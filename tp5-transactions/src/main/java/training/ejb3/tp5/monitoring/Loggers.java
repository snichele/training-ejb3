package training.ejb3.tp5.monitoring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Loggers {

    public final static Logger FUNCTIONAL_LOGGER = LoggerFactory.getLogger("FUNCTIONAL_LOGGER");
    public final static Logger EJB_MONITORING_LOGGER = LoggerFactory.getLogger("EJB_MONITORING_LOGGER");
    public final static Logger EJB_PERFORMANCE_LOGGER = LoggerFactory.getLogger("EJB_PERFORMANCE_LOGGER");
}
