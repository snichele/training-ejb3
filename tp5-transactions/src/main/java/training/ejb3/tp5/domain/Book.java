package training.ejb3.tp5.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import static training.ejb3.tp5.monitoring.Loggers.*;

@Entity
public class Book implements Serializable {

  @Id
  private String isbn;
  private String title;
  private String author;

  public Book() {
  }

  public Book(String isbn, String title, String author) {
    this.isbn = isbn;
    this.title = title;
    this.author = author;
  }

  @PrePersist
  public void prePersist(){
    EJB_MONITORING_LOGGER.debug("About to persists Book instance {}", this);
  }

  @PreRemove
  public void preRemove(){
    EJB_MONITORING_LOGGER.debug("About to remove Book instance {}", this);
  }

  @PreUpdate
  public void preUpdate(){
    EJB_MONITORING_LOGGER.debug("About to update Book instance {}", this);
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 83 * hash + (this.isbn != null ? this.isbn.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Book other = (Book) obj;
    if ((this.isbn == null) ? (other.isbn != null) : !this.isbn.equals(other.isbn)) {
      return false;
    }
    return true;
  }
}
