package training.ejb3.tp5.business;

import java.util.Collection;
import training.ejb3.tp5.domain.SavedCart;

public interface SavedCartsLocal {

  void saveCart(SavedCart cart);

  Collection<SavedCart> loadCartsForUser(String userId);
}
