package training.ejb3.tp5.business;

import java.util.Collection;
import training.ejb3.tp5.domain.Book;
import javax.ejb.Local;

@Local
public interface BookShopLocal extends BookShopCommon {
    void addBook(Book book);
    void addAllBooks(Collection<Book> books);
    void addBookFromBatch(Book book);
    void addBookThatWillNotWork(Book book);
}
