package training.ejb3.tp5.business.ejb;

import training.ejb3.tp5.business.BookShopRemote;
import training.ejb3.tp5.business.BookShopLocal;
import training.ejb3.tp5.domain.Book;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import static training.ejb3.tp5.monitoring.Loggers.*;

@Stateless
public class BookShopBean implements BookShopRemote, BookShopLocal {

  @PersistenceContext(unitName = "EJB3-TP3-PU")
  private EntityManager entityManager;

  @Override
  @TransactionAttribute(TransactionAttributeType.SUPPORTS)
  public Collection<Book> getAllBooks() {
    Query query = entityManager.createQuery("SELECT b from Book as b");
    return query.getResultList();
  }

  @Override
  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public void addBook(Book book) {
    FUNCTIONAL_LOGGER.info("Saving a book in database {}", book);
    entityManager.persist(book);
  }

  @Override
  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public void addAllBooks(Collection<Book> books) {
    for (Book book : books) {
      entityManager.persist(book);
    }
  }

  @Override
  @TransactionAttribute(TransactionAttributeType.MANDATORY)
  public void addBookFromBatch(Book book) {
    FUNCTIONAL_LOGGER.info("Saving a book in database from batch context {}", book);
    entityManager.persist(book);
  }

  @Override
  @TransactionAttribute(TransactionAttributeType.NEVER)
  public void addBookThatWillNotWork(Book book) {
    FUNCTIONAL_LOGGER.info("Saving a book in database and creates funny variants {}", book);
    try {
      entityManager.persist(book);
      entityManager.persist(variantOf(book));
    } catch (Exception e) {
      FUNCTIONAL_LOGGER.error("An exception occured because you where trying to save something in database in a non transactionnal context with a CMP !", e);
    }
  }

  @PostConstruct
  private void postContruct() {
    EJB_MONITORING_LOGGER.info("BookShopBean {} ready to be used.", this);
  }

  @PreDestroy
  private void preDestroy() {
    EJB_MONITORING_LOGGER.info("BookShopBean {} is about to be destroyed.", this);
  }

  private Book variantOf(Book book) {
    return new Book(book.getIsbn() + "-1", book.getTitle().toUpperCase(), book.getAuthor());
  }
}
