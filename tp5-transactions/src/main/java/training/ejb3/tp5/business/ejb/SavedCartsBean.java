package training.ejb3.tp5.business.ejb;

import training.ejb3.tp5.domain.SavedCart;
import training.ejb3.tp5.business.SavedCartsLocal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class SavedCartsBean implements SavedCartsLocal {

  @PersistenceContext(unitName = "EJB3-TP3-PU")
  private EntityManager entityManager;

  @Override
  public void saveCart(SavedCart cart) {
    entityManager.persist(cart);
  }

  @Override
  public Collection<SavedCart> loadCartsForUser(String userId) {
    Query query = entityManager.createQuery("SELECT c from SavedCart as c where c.userId =:userId").setParameter("userId", userId);
    return query.getResultList();
  }
}
