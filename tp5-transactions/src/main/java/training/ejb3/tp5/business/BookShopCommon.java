package training.ejb3.tp5.business;

import java.util.Collection;
import training.ejb3.tp5.domain.Book;

public interface BookShopCommon {

    Collection<Book> getAllBooks();
}
