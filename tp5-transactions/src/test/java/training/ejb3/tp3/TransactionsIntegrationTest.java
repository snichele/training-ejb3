package training.ejb3.tp3;

import java.util.ArrayList;
import training.ejb3.tp5.business.BookShopLocal;
import training.ejb3.tp5.domain.Book;
import training.ejb3.tp5.business.SavedCartsLocal;
import training.ejb3.tp5.domain.SavedCart;
import java.util.Calendar;
import java.util.Collection;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import java.util.Properties;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.transaction.UserTransaction;
import static org.junit.Assert.*;
import org.junit.Test;

public class TransactionsIntegrationTest {

  @Resource
  private UserTransaction userTransaction;
  @EJB
  BookShopLocal bookShop;
  @EJB
  SavedCartsLocal savedCarts;

  @Test
  public void test() throws Exception {

    Book book0 = new Book("AAAAAA", "UNIT TEST BOOK", "TRAINING");
    Book book1 = new Book("BBBBBB", "UNIT TEST BOOK 2", "TRAINING");
    Book book2 = new Book("CCCCCC", "UNIT TEST BOOK 3", "TRAINING");
    Book book3 = new Book("DDDDDD", "UNIT TEST BOOK 4", "TRAINING");
    Book book4 = new Book("EEEEEE", "Unit test book 4", "TRAINING");
    Collection<Book> books = new ArrayList<Book>() {
      {
        add(new Book("XXXXXX", "XAZEAZEAZE AZE ", "TRAINING"));
        add(new Book("YYYYYYY", "AZE adzDZA daz AZD", "TRAINING"));
        add(new Book("AAAAAA", "UNIT TEST BOOK", "TRAINING"));
      }
    };

    Context context = EJBContainer.createEJBContainer(datasourcePropertiesForOpenEJBContainer()).getContext();
    context.bind("inject", this);

    // Appellez les différentes méthodes addXXX de bookShop ci-après et vérifiez avec des assert le bon comportement.
    
  }

  private Properties datasourcePropertiesForOpenEJBContainer() {
    Properties p = new Properties();
    p.put("booksDatabase", "new://Resource?type=DataSource");
    p.put("booksDatabase.JdbcDriver", "org.hsqldb.jdbcDriver");
    p.put("booksDatabase.JdbcUrl", "jdbc:hsqldb:mem:bookdb");
    return p;
  }
}
