package training.ejb3.tp2;

import java.util.Properties;
import javax.ejb.embeddable.EJBContainer;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.BeforeClass;

public class BookShopBeanIntegrationTest {

  static EJBContainer container;

  static final String OPENEJB_JNDI_NAME_FOR_BOOKSHOP_EJB =
          "java:global/tp2-statefull-session-bean/BookShopBean!training.ejb3.tp2.BookShopLocal";
  static final String OPENEJB_JNDI_NAME_FOR_BOOKSHOPCART_EJB =
          "java:global/tp2-statefull-session-bean/BookShopCartBean";

  @BeforeClass
  public static void bootContainerAndLookupBean() throws Exception {
    container = EJBContainer.createEJBContainer(propertiesForConfiguringOpenEJBStatefulBeanManagment());
  }

  @AfterClass
  public static void shutdownContainer() throws Exception {
    container.close();
  }

  private static Properties propertiesForConfiguringOpenEJBStatefulBeanManagment() {
    Properties p = new Properties();
    p.put("MySTATEFUL", "new://Container?type=STATEFUL");
    p.put("MySTATEFUL.Capacity", "2"); //How many instances of Stateful beans can our server hold in memory?
    p.put("MySTATEFUL.Frequency", "1"); //Interval in seconds between checks
    p.put("MySTATEFUL.BulkPassivate", "0"); //No bulkPassivate - just passivate entities whenever it is needed
    return p;
  }

  @Test
  public void testStatefulBean() throws Exception {
    // Ecrivez votre code ici en mettant en oeuvre ce qui a été vu dans le cour
  }
}