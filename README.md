#Formation EJB 3

Ce repository git contient :

- les énoncés des travaux pratiques qui illustrent la formation EJB 3 (ci-après)
- les sources nécessaires au bon démarrage des différents travaux pratiques (onglet 'Sources', branche 'master')
- les solutions des travaux pratiques (onglet 'Sources', branche 'solution')

##Pré-requis et outils pour les travaux pratiques

Java doit être installé sur le poste.

A télécharger sur [Java SE Download page](http://www.oracle.com/technetwork/java/javase/downloads/) au besoin.

L'IDE Netbeans sera utilisé lors de cette formation pour son excellent support des
standards Java JEE.

A télécharger sur [NetBeans IDE X.X.X Download](https://netbeans.org/downloads/) **(version Java EE incluant
le serveur d'application glassfish)**

Afin de rester IDE-agnostique et de faciliter la gestion des dépendences vers des librairies
tierces, les modules créé pour chaque TP se baseront sur Maven (structure projet, mécanismes de build...). Netbeans
supporte de façon native les projets Maven.

##Travaux pratiques

### Généralités

Vous pouvez récupérer les sources de démarrage des différents travaux pratiques
en clonant ce repository git ou en téléchargeant l'archive des sources
(onglet 'Downloads', puis onglet 'Branches').

Que vous cloniez ou téléchargiez les sources, il existe deux branches :

- **master** : les sources de démarrage des tps
- **solutions** : les solutions des tps

Afin de faciliter l'apprentissage pratique, il est recommandé de :

- Créer un répertoire  `/formation-ejb3`
- Y créer un sous-répertoire de travail `/exercices`, nommé ci-après **répertoire de travail**

    - Déposez les sources de démarrage des tps dans ce répertoire
    - Effectuez vos développements dans celui-ci
    - Veillez à travaillez dans les sources de démarrage correspondant au bon TP.

- Y créer un sous-répertoire `/solutions` dans lequel vous pourrez déposer les solutions
pour pouvoir les étudier et les tester.

### TP 1 - EJB Session Stateless

#### Objectifs

Développer un premier EJB Session avec une interface Remote et Locale. Mettre en oeuvre
les annotations du cycle de vie et utilisez les Interceptors.

#### Pré-requis

Vous devez avoir installé Netbeans (versions utilisées dans ce TP : 7.3 en langue anglaise)
et commencer à développer à partir des sources de démarrage fournies.

#### Enoncé

Il s'agit du tp `tp1-stateless-session-bean`

- Lancez Netbeans
- File > Open Project > Sélectionnez le répertoire `tp1-stateless-session-bean` dans votre répertoire de travail.
- Le projet doit s'ouvrir dans l'onglet Projects de l'IDE
    ![screenshot](https://bitbucket.org/snichele/training-ejb3/raw/solution/tp1-project-structure.jpg)

    - *Source Packages* > contient les sources java.
    - *Test Packages* > contient les sources java des tests unitaires.
    - *Other Sources* > contient les fichiers non compilés faisant partie.
      du projet (configuration, scripts...). Les fichiers apparaissants dans Other Sources > src/main/resources
      seront inclus à la racine du classpath.
    - *Other Test Sources* > idem, mais pour les tests uniquement.
    - *Dependencies* > présente les librairies java tierces utilisées par le projet pour la compilation et l'execution du projet.
    - *Test Dependencies* > présente les librairies java tierces utilisées par les test unitaires en plus des librairies de base.
    - *Project Files* référence les fichiers propres à Maven (pom.xml...) Nous ne nous attarderons pas
    sur les fichiers de configuration maven plus que nécessaire dans la présente formation.

- Ecrivez le code java pour réaliser le service métier suivant :

    >Créer un service de gestion de stock de livres permettant d'ajouter de nouveaux
    >livres et d'obtenir la liste des livres existants. Ce service doit pouvoir être
    >accessible en local(même JVM) par d'autres EJB pour ajouter et consulter, mais uniquement
    >en consultation pour les futurs clients EJB distants.

    1. Créer une classe `Book`
        - elle doit posséder à minima les propriétés ISBN, titre, auteur
        - générez (ALT+INSERT) les méthodes equals/hashcode en vous appuyant sur l'ISBN
    2. Créer les interfaces métiers `BookShopLocal` (locale) et `BookShopRemote` (remote) répondant au besoin
    3. Créer un ejb `BookShopBean` implémentant ces interfaces.
        - Dans un premier temps, l'implémentation de l'ajout et de récupération peut
        se faire en "simulant" une base de données : stocker les instances de livres dans une collection
        java standard (`Collection<Book>`).
        - **Attention** : Réfléchissez bien au cycle de vie des beans pour implémenter la collection de stockage des livres...
        vous devriez pouvoir trouvez une implémentation correcte en vous appuyant sur le cours.

- Tester votre code

    Tester votre code peut se faire de plusieurs façons :

    - test purement unitaire, s'appuyant sur des mocks, hors de tout contexte de container.
    - test d'intégration, visant à vérifier que vos beans sont correctement déclarés et que l'injection
    de dépendances s'effectue bien.

    Nous allons mettre en pratique la deuxième approche, celle du test d'intégration.

    Un test d'intégration d'EJB implique :

    - de démarrer un container d'EJB dans le test unitaire
    - d'y déployer nos EJBs
    - de récupérer une référence à l'EJB que l'on souhaite tester à partir du contexte
    du test unitaire
    - d'appeller les méthodes de l'EJB et de contrôler le résultat.

    Il existe plusieurs conteneurs pouvant se prêter aux tests d'integration. OpenEJB, un conteneur d'EJB de la
    communauté Apache, propose une implémentation légère utilisable en contexte de test. OpenEJB est déjà référencé
    sur votre projet.

    Complétez le test unitaire `BookShopBeanIntegrationTest.java` :

      - Etudiez le code du test pour comprendre ce qui est fait (remplacez au besoin
        les noms / chemins de classes par vos propres classes).
      - Pour exécuter le test, effectuez un clic droit sur la classe > Test File


- Utilisation des méthodes de callback et des Interceptors

    - Ajoutez des méthodes de cycle de vie sur votre `BookShopBean` pour tracer
        la création et destruction d'instance dans les logs.
		
		- `@PostConstruct`
		- `@PreDestroy`
				
    - Créez un intercepteur destiné à tracer certains appels de méthode,
    dans une classe *LoggingInterceptor*, sur le modèle suivant

            public class LoggingInterceptor {
                private static final Logger LOGGER = Logger.getLogger(LoggingInterceptor.class.getName());

                @AroundInvoke
                public Object log(InvocationContext ctx) throws Exception {
                    LOGGER.info("*** LoggingInterceptor - start");

                    long start = System.nanoTime();
                    try {
                        return ctx.proceed();
                    } catch (Exception e) {
                        throw e;
                    } finally {
                        long time = System.nanoTime() - start;
                        String method = ctx.getClass().getName() + "." + ctx.getMethod().getName() + "()";
                        LOGGER.log(INFO, "*** LoggingInterceptor : l''invocation de {0} a pris {1} ms", new Object[]{method, time});
                    }
                }
            }

    - Branchez l'intercepteur sur des méthodes du `BookShopBean`
	
		- `@Interceptors(xxxxxxx.class)`
			
    - relancez le test automatisé et vérifiez la sortie console

#### A propos de la solution

		La solution du tp contient un test simulant plusieurs clients simultanés
		effectuants de nombreux appels a un EJB. Ce test permet ainsi de constater le remplissage du pool en fonction
		du nombre de clients (faire varier les paramètres du test, trace dans les logs grâce aux méthodes
		de callback).

### TP 2 - EJB Session statefull

#### Objectifs

Développer un EJB Session Statefull.

#### Pré-requis

Avoir réalisé le TP 1.
Développez à partir des sources de démarrage fournies en copiant éventuellement les classes développées dans le TP1.

#### Enoncé

Vous devez à présent créer un Stateful Session Bean `BookShopCartBean` qui va servir
de panier dans lequel le client de cet EJB pourra stocker ou enlever des livres renvoyés
par l'EJB Session Stateless développé dans le TP1 (Vous pouvez copier-coller les classes).

Ce bean doit exposer les fonctions suivantes :

    public void addBook(Book book)
    public void removeBook(Book book)
    public Collection<Book> getSelectedBooks()

Pensez à créer des fonctions de callback pour pouvoir observer le comportement du Bean.

Compléter le test unitaire `BookShopBeanIntegrationTest` et manipulez votre Bean pour observer
les appels aux différentes fonctions de callback.

Pour vous aider :

  - `Thread.sleep()` permet de mettre le thread courant en pause
  - `@StatefulTimeout` est une annotation permettant de contrôler la durée de vie précise d'une classe de stateful
    session bean.

### TP 3 - EJB Entity

#### Objectifs

Développer des EJB entités interconnectés.

#### Pré-requis

Développez dans le répertoire de démarrage fournit, dans lequel vous aurez copié-collé les sources développées dans le TP précédent.

#### Enoncé

- Rendre la classe `Book` persistante

	- la transformer en un EJB Entity
	- Ajouter les éléments nécessaires à la bonne gestion de la persistance de celle-ci (annotations, attributs, etc...)
	
- Créer un EJB entity `BookShopCartEntity` représentant une version persistante du `BookShopCartBean` développé dans le TP précédent. (Il n'est pas possible 
en effet de créer un bean qui soit à la fois Stateful *et* Entity). La classe `BookShopCartEntity` doit contenir une collection de `Book` : pensez donc à utiliser la bonne annotation de persistence (OneToMany, ManyToOne, OneToOne ? Choisissez la bonne) pour que la collection soit bien persistée.

- Supprimer la référence dans votre classe `BookShopBean` vers l'EJB simulant une base de données, et remplacez là par

		  @PersistenceContext(unitName = "EJB3-TP3-PU")
		  private EntityManager entityManager;
		  
	Corrigez votre code afin d'utiliser l'entityManager pour accéder à la base de données.


- Une fois le mapping et les ajustements de code réalisés, testez dans la classe de test :

	- un enregistrement de plusieurs livres puis le rechargement de ceux-ci
	- la sauvegarde puis le rechargement d'une instance de `BookShopCartEntity`

### TP 4 - Message Driven Bean

#### Objectifs

Comprendre le fonctionnement d'un MDB.

#### Pré-requis

Avoir récupéré les sources de démarrage du TP 4

#### Enoncé

Le développement d'un MDB restant assez fastidieux et très _technique_, nous nous contenterons d'illustrer  son utilisation : analyser le code
fournit qui correspond à un implémentation complète d'un test de mise en oeuvre d'un MDB.

### TP 5 - Gestion des transactions

#### Objectifs

Mettre en oeuvre plusieurs cas d'utilisation des transactions.

#### Pré-requis

Avoir récupéré les sources de démarrage du TP 5

#### Enoncé

Etudiez les sources fournies (très similaires à ce que vous avez pu développer dans les TPS précédents).

Remarquez en particulier la classe `BookShopBean` qui déclare plusieurs méthodes `addXXXX` avec des `@TransactionAttribute` différents.

Compléter la classe de test unitaire en invoquant les variantes de `addXXX` et en vous assurant de bien comprendre ce qui se passe.

- Remarquez que la classe de test est *liée* au conteneur d'EJB (l'appel de `bind`), lui octroyant la possibilité de bénéficier des mécanismes d'injection des dépendances
- Vous pouvez utiliser l'attribut `userTransaction` pour démarrer / rollbacker / commiter une transaction au besoin (nécessaire pour au moins l'une des fonctions addXXX)


##A propos des solutions

Une fois le code source des solutions récupéré, les différents exemples / solutions peuvent être executés
de la façon suivante :

- training-ejb3-flash-sells-EAR

    - Solution mettant en oeuvre l'ensemble des principes abordés :

        - EAR pour le déploiement coté serveur
        - module web pour le frontal web
        - module ejb contenant les EJB (session, mdb...)
        - module swing autonome se connectant via un lookup jndi aux EJB déployés

    - Pour tester cette solution

        - Construire (mvn clean install) l'application à partir de la racine
        - Lancer glassfish sur la machine locale

            - L'application référence des datasources qui *doivent être déclarées sur le serveur* :
            le fichier training-ejb3-flash-sells-ejb\src\main\resources\serverConfig\glassfish-resources.xml
            contient les informations nécessaires à cette opération.


        - Déployer l'ear obtenu sur une instance de glassfish démarrée sur la machine locale (version 3.1 testée)
        - Accéder à l'application via un navigateur web à l'adresse précisée dans le pom du module ear
        - Executer le client Swing (classe MainFrame) pour illustrer un accès à un Bean en remote.