package training.ejb3.tp4;

import java.util.Calendar;
import javax.ejb.embeddable.EJBContainer;
import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

public class MDBIntegrationTest extends TestCase {

  @Resource
  private ConnectionFactory connectionFactory;

  @Resource(name = "DeliveryBean")
  private Queue deliveryBeanQueue;

  @Resource(name = "AnswerQueue")
  private Queue answerQueue;

  public void test() throws Exception {
    EJBContainer.createEJBContainer().getContext().bind("inject", this);

    Session session = createJMSConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);

    MessageProducer questions = session.createProducer(deliveryBeanQueue);
    MessageConsumer answers = session.createConsumer(answerQueue);

    SavedCart savedCart = new SavedCart("USER_TEST", Calendar.getInstance().getTime());
    savedCart.setId(4807l);

    sendSavedCartForShipping(savedCart, questions, session);

    assertEquals("SavedCart 4807 treated", receiveText(answers));

  }

  private void sendSavedCartForShipping(SavedCart savedCart, MessageProducer questions, Session session) throws JMSException {
    questions.send(session.createObjectMessage(savedCart));
  }

  private Connection createJMSConnection() throws JMSException{
    Connection connection = connectionFactory.createConnection();
    connection.start();
    return connection;
  }

  private String receiveText(MessageConsumer answers) throws JMSException {
    Message message = answers.receive(1000);
    return ((TextMessage) message).getText();
  }
}
