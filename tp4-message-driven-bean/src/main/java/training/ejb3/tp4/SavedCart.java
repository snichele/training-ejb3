package training.ejb3.tp4;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.*;
import static training.ejb3.tp4.Loggers.EJB_MONITORING_LOGGER;

@Entity
public class SavedCart implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String userId;
  @Temporal(TIMESTAMP)
  private Date savedAt;
  @OneToMany(fetch = FetchType.EAGER)
  private Collection<Book> selectedBooks = new ArrayList<Book>();

  public SavedCart() {
  }

  public SavedCart(String userId, Date savedAt) {
    this.userId = userId;
    this.savedAt = savedAt;
  }

  @PrePersist
  public void prePersist() {
    EJB_MONITORING_LOGGER.debug("About to persists SavedCart instance {}", this);
  }

  @PreRemove
  public void preRemove() {
    EJB_MONITORING_LOGGER.debug("About to remove SavedCart instance {}", this);
  }

  @PreUpdate
  public void preUpdate() {
    EJB_MONITORING_LOGGER.debug("About to update SavedCart instance {}", this);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getSavedAt() {
    return savedAt;
  }

  public void setSavedAt(Date savedAt) {
    this.savedAt = savedAt;
  }

  public void addBookToCart(Book book) {
    this.selectedBooks.add(book);
  }

  public Collection<Book> getSelectedBooks() {
    return Collections.unmodifiableCollection(selectedBooks);
  }

  public void setSelectedBooks(Collection<Book> selectedBooks) {
    this.selectedBooks = selectedBooks;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
