<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">

  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
  <div class="container">
    <h1>Bienvenue sur la plate-forme de vente flash !</h1>
    <h2>Produits actuellement référencés</h2>
    <ul>
      <c:forEach items="${products}" var="product">
        <li>
          ${product.label} [REF : ${product.reference}]
        </li>
      </c:forEach>
    </ul>
    <h2>Accédez aux offres spéciales</h2>
    <ul>
      <li><a href="<c:url value='/offres-standard' />">Standards</a></li>
      <li><a href="<c:url value='/offres-vip' />">VIP</a></li>
    </ul>
  </div>
</body>
</html>      