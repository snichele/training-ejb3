package training.ejb3.flashsells.web.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.ServletSecurity.TransportGuarantee;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import training.ejb3.flashsells.business.ProductsBeanRemote;
import training.ejb3.flashsells.domain.ClientCategory;

@WebServlet(name = "Offres", urlPatterns = {"/offres-standard", "/offres-vip"})
// Compléter les roles ci après et décommentez l'annotation @ServletSecurity pour l'activer
//@ServletSecurity(
//        @HttpConstraint(
//        transportGuarantee = TransportGuarantee.CONFIDENTIAL,
//        rolesAllowed = {????}))
public class Offres extends HttpServlet {

  
  private ProductsBeanRemote productsBean;

  protected void processRequest(
          HttpServletRequest request,
          HttpServletResponse response) throws ServletException, IOException {

    String userPath = request.getServletPath();

    if (userPath.equals("/offres-standard")) {
      request.setAttribute("offers", productsBean.getAllStandardsOffers());
      request.setAttribute("clientCategory", ClientCategory.STANDARD);
    }else {
      request.setAttribute("offers", productsBean.getAllVIPOffers());
      request.setAttribute("clientCategory", ClientCategory.VIP);
    }

    request.getRequestDispatcher("offres.jsp").forward(request, response);
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  public String getServletInfo() {
    return "This is the Home servlet";
  }
}
