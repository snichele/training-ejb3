package training.ejb3.flashsells.web.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import training.ejb3.flashsells.business.ProductsBeanRemote;

@WebServlet(name = "Home", urlPatterns = {"/Home"})
public class Home extends HttpServlet {

  private ProductsBeanRemote productsBean;

  protected void processRequest(
          HttpServletRequest request,
          HttpServletResponse response) throws ServletException, IOException {

    request.setAttribute("products", productsBean.getAllReferencedProducts());
    request.getRequestDispatcher("home.jsp").forward(request, response);
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  public String getServletInfo() {
    return "This is the Home servlet";
  }
}
