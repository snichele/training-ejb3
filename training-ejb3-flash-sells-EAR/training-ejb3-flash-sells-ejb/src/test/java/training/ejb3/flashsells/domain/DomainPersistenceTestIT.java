package training.ejb3.flashsells.domain;

import training.ejb3.flashsells.domain.jpa.Product;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DomainPersistenceTestIT {

    private static EntityManagerFactory emf;
    private static EntityManager entityManager;
    private static final String INTEGRATION_TESTING_PERSISTENCE_UNIT_NAME = "ITPU";

    @BeforeClass
    public static void initTestFixture() throws Exception {
        emf = Persistence.createEntityManagerFactory(INTEGRATION_TESTING_PERSISTENCE_UNIT_NAME);
        entityManager = emf.createEntityManager();
    }

    @AfterClass
    public static void closeTestFixture() {
        entityManager.close();
        emf.close();
    }

    @Test
    public void product_can_be_saved_and_fetched_back() {
        entityManager.getTransaction().begin();
        entityManager.persist(new Product("TEST00", "Test product"));
        List productsFromDB = entityManager.createQuery("from Product").getResultList();
        Assert.assertEquals(1, productsFromDB.size());
        Assert.assertEquals("Test product", ((Product) productsFromDB.get(0)).getLabel());
        entityManager.getTransaction().commit();
    }
}