package training.ejb3.flashsells.init;

import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import training.ejb3.flashsells.domain.ClientCategory;

import training.ejb3.flashsells.domain.jpa.Offer;
import training.ejb3.flashsells.domain.jpa.Product;
import training.ejb3.flashsells.monitoring.Loggers;

// Annotez cette classe pour la transformer en EJB singleton et la charger au démarrage pour qu'elle initialise la base de données
public class InitDB {

  // Injectez le persistence context par annotation
  private EntityManager entityManager;

  @PostConstruct
  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public void initDB() {
    Loggers.EJB_MONITORING_LOGGER.info("Initializing DB...");
    Loggers.EJB_MONITORING_LOGGER.info("Creating some starting products");
    Product p1 = newProduct("AZE", "This is stub product 01");
    Product p2 = newProduct("AZR", "This is stub product 02");
    Product p3 = newProduct("AZT", "This is stub product 03");
    entityManager.persist(p1);
    entityManager.persist(p2);
    entityManager.persist(p3);

    Loggers.EJB_MONITORING_LOGGER.info("Creating some special offers");
    Offer o1 = new Offer(p1, now(), tommorow(), ClientCategory.STANDARD);
    Offer o2 = new Offer(p2, now(), tommorow(), ClientCategory.VIP);
    entityManager.persist(o1);
    entityManager.persist(o2);

  }

  private Product newProduct(String ref, String desc) {
    return new training.ejb3.flashsells.domain.jpa.Product(ref, desc);
  }

  private Date now() {
    return Calendar.getInstance().getTime();
  }

  private Date tommorow() {
    return new Date(now().getTime() + (24 * 60 * 60 * 1000));
  }
}
