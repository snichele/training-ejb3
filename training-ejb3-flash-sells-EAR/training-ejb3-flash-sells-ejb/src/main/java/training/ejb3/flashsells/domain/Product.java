package training.ejb3.flashsells.domain;

public interface Product {

    String getReference();
    String getLabel();
}
