package training.ejb3.flashsells.business;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import training.ejb3.flashsells.domain.Product;

@MessageDriven(mappedName = "jms/NewProduct", activationConfig = {
  @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
  @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
/**
 * This class is intended to receive remote JMS clients call.
 * This functionality is not yet implemented and this class not tested.
 */
public class NewProductBean implements MessageListener {

  @Resource
  MessageDrivenContext mdc;
  @PersistenceContext(unitName = "EJB3-FLASHSELLS-PRODUCTS-PU")
  private EntityManager em;
  @Resource
  private javax.transaction.UserTransaction utx;

  public NewProductBean() {
  }

  @Override
  public void onMessage(Message message) {
    ObjectMessage msg = null;
    try {
      if (message instanceof ObjectMessage) {
        if (message instanceof ObjectMessage) {
          Product e = (Product) ((ObjectMessage) message).getObject();
          save(e);
        }
      }
    } catch (JMSException e) {
      e.printStackTrace();
      mdc.setRollbackOnly();
    } catch (Throwable te) {
      te.printStackTrace();
    }
  }

  public void save(Object object) {
    try {
      utx.begin();
      em.persist(object);
      utx.commit();
    } catch (Exception e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
      throw new RuntimeException(e);
    }
  }
}
