package training.ejb3.flashsells.domain.jpa;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NamedQuery(
        name = "findAllProducts",
        query = "select p from Product p")
@Getter
@NoArgsConstructor
public class Product implements training.ejb3.flashsells.domain.Product, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String reference;
    private String label;

    public Product(String reference, String label) {
        this.reference = reference;
        this.label = label;
    }
    private static final long serialVersionUID = 1L;
}
