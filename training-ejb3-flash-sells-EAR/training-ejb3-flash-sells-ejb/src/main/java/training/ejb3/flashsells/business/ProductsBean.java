package training.ejb3.flashsells.business;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import training.ejb3.flashsells.domain.ClientCategory;
import training.ejb3.flashsells.domain.Product;
import training.ejb3.flashsells.domain.jpa.Offer;

@Stateless(mappedName = "ejb/ProductsBean")
public class ProductsBean implements ProductsBeanRemote {

  @PersistenceContext(unitName = "EJB3-FLASHSELLS-PRODUCTS-PU")
  private EntityManager entityManager;

  @Override
  public List<Product> getAllReferencedProducts() {
    return (List<Product>) entityManager.createNamedQuery("findAllProducts").getResultList();
  }

  @Override
  @RolesAllowed("VIP")
  public List<Offer> getAllVIPOffers() {
    return (List<Offer>) entityManager.createQuery("select o from Offer o where o.forCategory=:category").setParameter("category", ClientCategory.VIP).getResultList();
  }

  @Override
  @RolesAllowed({"STANDARD","VIP"})
  public List<Offer> getAllStandardsOffers() {
        return (List<Offer>) entityManager.createQuery("select o from Offer o where o.forCategory=:category").setParameter("category", ClientCategory.STANDARD).getResultList();
  }
}
