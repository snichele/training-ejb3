package training.ejb3.flashsells.domain.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import training.ejb3.flashsells.domain.ClientCategory;

@Entity
@Getter
@NoArgsConstructor
public class Offer implements Serializable {

  @Id
  @GeneratedValue
  private Long id;
  @OneToOne
  private Product concerning;
  @Temporal(javax.persistence.TemporalType.DATE)
  private Date startAt;
  @Temporal(javax.persistence.TemporalType.DATE)
  private Date endsAt;
  private ClientCategory forCategory;

  public Offer(Product concerning, Date startAt, Date endsAt, ClientCategory forCategory) {
    this.concerning = concerning;
    this.startAt = startAt;
    this.endsAt = endsAt;
    this.forCategory = forCategory;
  }
}
