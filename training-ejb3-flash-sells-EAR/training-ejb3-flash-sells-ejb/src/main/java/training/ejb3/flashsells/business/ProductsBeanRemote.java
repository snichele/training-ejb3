package training.ejb3.flashsells.business;

import java.util.List;
import javax.ejb.Remote;
import training.ejb3.flashsells.domain.Product;
import training.ejb3.flashsells.domain.jpa.Offer;

@Remote
public interface ProductsBeanRemote {

    List<Product> getAllReferencedProducts();
    List<Offer> getAllVIPOffers();
    List<Offer> getAllStandardsOffers();
}
