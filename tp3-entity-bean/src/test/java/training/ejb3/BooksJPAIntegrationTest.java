package training.ejb3;

import junit.framework.TestCase;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import java.util.Properties;

public class BooksJPAIntegrationTest extends TestCase {

  public void test() throws Exception {

    Context context = EJBContainer.createEJBContainer(datasourcePropertiesForOpenEJBContainer()).getContext();

    // Ecrivez votre test ici.
  }

  private Properties datasourcePropertiesForOpenEJBContainer() {
    Properties p = new Properties();
    p.put("movieDatabase", "new://Resource?type=DataSource");
    p.put("movieDatabase.JdbcDriver", "org.hsqldb.jdbcDriver");
    p.put("movieDatabase.JdbcUrl", "jdbc:hsqldb:mem:moviedb");
    return p;
  }
}
