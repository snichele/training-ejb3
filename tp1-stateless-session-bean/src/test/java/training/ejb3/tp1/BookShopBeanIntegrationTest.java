package training.ejb3.tp1;

import javax.ejb.embeddable.EJBContainer;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.BeforeClass;

public class BookShopBeanIntegrationTest {

    static EJBContainer container;
    // static BookShopLocal bookshop;

    // EJB specification does not specify clearly which rule to use for jndi naming
    // so we must leave with disparities between the containers...
    static final String OPENEJB_JNDI_NAME_FOR_BOOKSHOP_EJB =
            "java:global/tp1-stateless-session-bean/BookShopBean!training.ejb3.tp1.BookShopLocal";

    @BeforeClass
    public static void bootContainerAndLookupBean() throws Exception {
        container = EJBContainer.createEJBContainer();
        // bookshop = (BookShopLocal) container.getContext().lookup(OPENEJB_JNDI_NAME_FOR_BOOKSHOP_EJB);
    }

    @AfterClass
    public static void shutdownContainer() throws Exception {
        container.close();
    }

    @Test
    public void testAddBook() throws Exception {
      // Ecrivez votre code ici.
    }
}